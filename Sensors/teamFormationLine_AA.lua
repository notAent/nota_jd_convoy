local sensorInfo = {
	name = "teamFormationLine",
	desc = "Return team formation line",
	author = "PepeAmpere",
	date = "2017-11-10",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge -- cached forev4r

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return stuctured description of the formation
return function()
	return {
		name = "teamFormationLine_AA",
		positions = {
			[1] = Vec3(-4,0,0), [2] = Vec3(4,0,0),
			[3] = Vec3(-5,0,0), [4] = Vec3(5,0,0),
			[5] = Vec3(-4,0,-1), [6] = Vec3(4,0,-1),
			[7] = Vec3(-5,0,-1), [8] = Vec3(5,0,-1),
			[9] = Vec3(-4,0,1), [10] = Vec3(4,0,1),
			[11] = Vec3(-5,0,1), [12] = Vec3(5,0,1),
			[13] = Vec3(-6,0,1), [14] = Vec3(6,0,1),
		},		
		generated = false,
		defaults = {
			spacing = Vec3(70, 1 ,70),
			hillyCoeficient = 20,
			constrained = true,
			variant = false,
			rotable = true,
		},
	}
end