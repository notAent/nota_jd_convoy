function getInfo()
	return {
		fields = { "pos" }
	}
end

local function tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

local function distanceToLine(p1, p2, p3)

	local numerateur = math.abs((p1.z - p2.z)*p3.x - (p1.x - p2.x)*p3.z + p1.x*p2.z - p2.x*p1.z)
	local denumerateur = math.sqrt((p1.x - p2.x)^2 + (p1.z - p2.z)^2)

	return numerateur / denumerateur
end

return function(posPointMan, nextMinePos)

	local posPointMan = posPointMan
	local nextMinePos = nextMinePos
	
	local halfWidth = 100
	local halfHeitgh = 100
	
	local metalValue = 1
	local movingCost = 0
	local fromPathCost = 0
	
	local maxScore = 0
	local maxScoreIndice = 1
	
	local areasChracteristics = {}
	
	local directionVector = Vec3(nextMinePos.x - posPointMan.x, 0, nextMinePos.z - posPointMan.z)
	local nbrAreaToMine = math.floor(directionVector:Length() / (2*halfWidth))
	local norDirectionVector = directionVector:Normalize()
	local orthoDirectionVector = Vec3(-norDirectionVector.z, 0, norDirectionVector.x)
	local test = math.sqrt((directionVector.x)^2 + (directionVector.z)^2)
	--Spring.Echo(dump(nbrAreaToMine))
	
	for i = 0, (nbrAreaToMine+1) do
	
		for j = 0, 2 do
		
			-- local xloc = 2*i*halfWidth
			-- local zloc = 2*j*halfHeitgh
			-- local yloc = Spring.GetGroundHeight(xloc, zloc)
			local Xfactor = 2*i*halfWidth
			local Zfactor = 2*j*halfHeitgh
			-- Spring.Echo(dump(Zfactor))
			local Xdisplacement = Vec3(Xfactor*(norDirectionVector.x), 0, Xfactor*(norDirectionVector.z))
			local Zdisplacement = Vec3(Zfactor*(orthoDirectionVector.x), 0, Zfactor*(orthoDirectionVector.z))
			local center = posPointMan + Xdisplacement + Zdisplacement
			--Spring.Echo(dump(center))
			
			local scoreArea = 0
			
			local listFeaturesArea = Spring.GetFeaturesInRectangle(center.x-halfWidth, center.z-halfHeitgh, center.x+halfWidth, center.z+halfHeitgh)
			
			for k = 1, tablelength(listFeaturesArea) do 
			
				local remainingMetal,_,_,_,_ = Spring.GetFeatureResources(listFeaturesArea[k])
				scoreArea = scoreArea + remainingMetal
			
			end
			
			local areaScore = metalValue * scoreArea + movingCost * center:Distance(posPointMan) + fromPathCost * distanceToLine(posPointMan, nextMinePos, center)
			areasChracteristics[1+i+3*j] = center
			
			if areaScore > maxScore then
			
				maxScore = areaScore
				maxScoreIndice = 1+i+3*j
				
			end 
			
		end
	end
	
	--Spring.Echo(dump(maxScore))
	--Spring.Echo(dump(areasChracteristics[maxScoreIndice]))
	
	if maxScore > 0 then 
		return areasChracteristics[maxScoreIndice]
	else 
		return nil
	end
end