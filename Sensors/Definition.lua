local sensorInfo = {
	name = "Definition",
	desc = "Return definition of the formaiton based on name key",
	author = "PepeAmpere",
	date = "2017-05-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local formationDefinitions = {
	-- taken from NOE formations, \games\nota-xxx.sdd\LuaRules\Configs\noe\formations.lua
	["swarm"] = {
		name = "swarm",
		positions = {
			[1]  = {0,0},		[2]  = {9,-1},		[3]  = {2,-8},		[4]  = {-5,-7},		[5]  = {-10,4},
			[6]  = {1,10},		[7]  = {12,9},		[8]  = {16,-2},		[9]  = {12,-11},	[10] = {1,-17},
			[11] = {-8,-16},	[12] = {-15,-3},	[13] = {-15,10},	[14] = {-5,18},		[15] = {8,19},
			[16] = {21,13},		[17] = {25,2},		[18] = {21,-10},	[19] = {6,-20},		[20] = {-4,-22},
			[21] = {-17,-7},	[22] = {-22,2},		[23] = {-15,20},	[24] = {3,26},		[25] = {18,23},
			[26] = {29,10},		[27] = {28,-7},		[28] = {21,-20},	[29] = {5,-27},		[30] = {-13,-24},
		},
		generated = false,
		defaults = {
			spacing = Vec3(10, 1 ,10),
			hillyCoeficient = 30,
			constrained = true,
			variant = false,
			rotable = false,
		},		
	},
	["wedge"] = {
		name = "wedge",
		positions = {
			[1]  = {0,0},
			[2]  = {-1,2},		[3]  = {0,2},		[4]  = {1,2},
			[5]  = {-2,1},		[6]  = {-1,1},		[7]  = {0,1,},		[8]  = {1,1},		[9]  = {2,1},
			[10]  = {-3,0},		[11]  = {-2,0},		[12]  = {-1,0},		[13]  = {0,3},		[14]  = {1,0},		[15]  = {2,0},		[16]  = {3,0},
			[17]  = {-4,-1},	[18]  = {-3,-1},	[19]  = {-2,-1},    [20]  = {-1,-1},	[21]  = {0,-1},		[22]  = {1,-1},		[23]  = {2,-1},		[24]  = {3,-1},		[25]  = {4,-1},
			[26]  = {-5,-2},	[27]  = {-4,-2},    [28]  = {-3,-2},	[29]  = {-2,-2},    [30]  = {-1,-2},	[31]  = {0,-2},		[32]  = {1,-2},		[33]  = {2,-2},		[34]  = {3,-2},		[35]  = {4,-2},		[36]  = {5,-2},
		},		
		generated = false,
		defaults = {
			spacing = Vec3(80, 1 ,100),
			hillyCoeficient = 20,
			constrained = true,
			variant = false,
			rotable = true,
		},		
	},
	["diamond"] = {
		name = "diamond",
		positions = {
			[1]  = {0,0},
			[2]  = {1,0},		[3]  = {0,-1},		[4]  = {-1,0},
			[5]  = {0,1},		[6]  = {1,-1},		[7]  = {-1,-1},		[8]  = {-1,1},		[9]  = {1,1},
			[10]  = {0,2},		[11]  = {2,0},		[12]  = {0,-2},		[13]  = {-2,0},
		},		
		generated = false,
		defaults = {
			spacing = Vec3(50, 1 ,50),
			hillyCoeficient = 20,
			constrained = true,
			variant = false,
			rotable = true,
		},		
	},
	["inFrontLine"] = {
		name = "inFrontLine",
		positions = {
			[1]  = {0,30},
			[2]  = {-1,30},		[3]  = {1,30},		[4]  = {-2,30},
			[5]  = {2,30},		[6]  = {-3,30},		[7]  = {3,30},		[8]  = {-4,30},		[9]  = {4,30},
			[10]  = {-5,30},		[11]  = {5,30},		[12]  = {-6,30},		[13]  = {6,30},		[14]  = {0,29},		[15]  = {1,29},		[16]  = {-1,29},
			[17]  = {2,29},	[18]  = {-2,29},	[19]  = {3,29},    [20]  = {-3,29},	[21]  = {4,29},		[22]  = {-4,29},		[23]  = {5,29},		[24]  = {-5,29},		[25]  = {6,29},
			[26]  = {-6,29},
		},		
		generated = false,
		defaults = {
			spacing = Vec3(15, 1 ,15),
			hillyCoeficient = 20,
			constrained = true,
			variant = false,
			rotable = true,
		},		
	},
	["inBackLine"] = {
		name = "inBackLine",
		positions = {
			[1]  = {0,-30},
			[2]  = {-1,-30},		[3]  = {1,-30},		[4]  = {-2,-30},
			[5]  = {2,-30},		[6]  = {-3,-30},		[7]  = {3,-30},		[8]  = {-4,-30},		[9]  = {4,-30},
			[10]  = {-5,-30},		[11]  = {5,-30},		[12]  = {-6,-30},		[13]  = {6,-30},		[14]  = {0,-29},		[15]  = {1,-29},		[16]  = {-1,-29},
			[17]  = {2,-29},	[18]  = {-2,-29},	[19]  = {3,-29},    [20]  = {-3,-29},	[21]  = {4,-29},		[22]  = {-4,-29},		[23]  = {5,-29},		[24]  = {-5,-29},		[25]  = {6,-29},
			[26]  = {-6,-29},	
		},		
		generated = false,
		defaults = {
			spacing = Vec3(15, 1 ,15),
			hillyCoeficient = 20,
			constrained = true,
			variant = false,
			rotable = true,
		},		
	},
	["inRightLIne"] = {
		name = "inRightLIne",
		positions = {
			[1]  = {30,0},
			[2]  = {30,-1},		[3]  = {30,1},		[4]  = {30,-2},
			[5]  = {30,2},		[6]  = {30,-3},		[7]  = {30,3},		[8]  = {30,-4},		[9]  = {30,4},
			[10]  = {30,-5},		[11]  = {30,5},		[12]  = {30,-6},		[13]  = {30,6},		[14]  = {29,0},		[15]  = {29,1},		[16]  = {29,-1},
			[17]  = {29,2},	[18]  = {29,-2},	[19]  = {29,3},    [20]  = {29,-3},	[21]  = {29,4},		[22]  = {29,-4},		[23]  = {29,5},		[24]  = {29,-5},		[25]  = {29,6},
			[26]  = {29,-6},	
		},		
		generated = false,
		defaults = {
			spacing = Vec3(15, 1 ,15),
			hillyCoeficient = 20,
			constrained = true,
			variant = false,
			rotable = true,
		},		
	},
	["inLeftLine"] = {
		name = "inLeftLine",
		positions = {
			[1]  = {-30,0},
			[2]  = {-30,-1},		[3]  = {-30,1},		[4]  = {-30,-2},
			[5]  = {-30,2},		[6]  = {-30,-3},		[7]  = {-30,3},		[8]  = {-30,-4},		[9]  = {-30,4},
			[10]  = {-30,-5},		[11]  = {-30,5},		[12]  = {-30,-6},		[13]  = {-30,6},		[14]  = {-29,0},		[15]  = {-29,1},		[16]  = {-29,-1},
			[17]  = {-29,2},	[18]  = {-29,-2},	[19]  = {-29,3},    [20]  = {-29,-3},	[21]  = {-29,4},		[22]  = {-29,-4},		[23]  = {-29,5},		[24]  = {-29,-5},		[25]  = {-29,6},
			[26]  = {-29,-6},
		},		
		generated = false,
		defaults = {
			spacing = Vec3(15, 1 ,15),
			hillyCoeficient = 20,
			constrained = true,
			variant = false,
			rotable = true,
		},		
	},
	["inFrontLine_Cavalry"] = {
		name = "inFrontLine_Cavalry",
		positions = {
			[1]  = {0,15},
			[2]  = {-3,15},		[3]  = {3,15},		[4]  = {-5,15},
			[5]  = {5,15},		[6]  = {-7,15},		[7]  = {7,15},		[8]  = {0,13},		[9]  = {3,13},
			[10]  = {-3,13},		[11]  = {5,13},		[12]  = {-5,13},		[13]  = {7,13},		[14]  = {-7,13},		[15]  = {0,11},		[16]  = {3,11},
			[17]  = {-3,11},	[18]  = {-5,11},	[19]  = {5,11},    [20]  = {-7,11},	[21]  = {7,11},
		},		
		generated = false,
		defaults = {
			spacing = Vec3(25, 1 ,25),
			hillyCoeficient = 20,
			constrained = true,
			variant = false,
			rotable = true,
		},		
	},
	["inFrontLine_AA"] = {
		name = "inFrontLine_AA",
		positions = {
			[1]  = {3,8},
			[2]  = {-3,8},		[3]  = {5,8},		[4]  = {-5,8},
			[5]  = {5,6},		[6]  = {-5,6},		[7]  = {3,6},		[8]  = {-3,6},		[9]  = {2,8},
			[10]  = {-2,8},
		},		
		generated = false,
		defaults = {
			spacing = Vec3(34, 1 ,34),
			hillyCoeficient = 20,
			constrained = true,
			variant = false,
			rotable = true,
		},		
	},
}

-- @description return stuctured description of the formation
-- @argument formationName [string] name of the formaiton
return function(formationName)
	local thisDefinition = formationDefinitions[formationName]
	local thisPositions = thisDefinition.positions
	local vectorPositions = {}
	local vectorPositionsCount = 0
	
	for i=1, #thisPositions do
		vectorPositionsCount = vectorPositionsCount + 1
		vectorPositions[vectorPositionsCount] = Vec3(thisPositions[i][1], 0, -thisPositions[i][2]) -- 
	end
	
	-- do not rewrite the originial table otherwise it is not robust on "reset"
	local finalDefinition = {
		name = thisDefinition.name,
		positions = vectorPositions,
		generated = thisDefinition.generated,
		defaults = thisDefinition.defaults,		
	}
	
	return finalDefinition
end