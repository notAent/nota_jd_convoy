local sensorInfo = {
	name = "Form_InfantryGroup",
	desc = "Form 4 equivalent groups",
	author = "DeslogisJocelyn",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back
-- @argument sourceFormation [array of Vec3] list of positions representing input formaiton  
-- @argument heading [number,Vec3] heading or position vector towards which we want to rotate the formation, Vec3 makes sense only if center position is used as well
-- @argument centerPosition [Vec3|optional] if passed, it means we want to get absolute positions on return with this position as center of the formation
-- @argument spacing [Vec3|optional] if passed, represents modification of norm source position in each dimension
return function(firstGroup, secondGroup, thirdGroup, fourthGroup, closestMetalSpot, squadFormation)

	if (bb.convoyHeading == nil) then 
		
		local groupArray = {firstGroup, secondGroup, thirdGroup, fourthGroup}
		
		return groupArray
	else
	
		-- Calculate the positions of pointmen
		local firstminIndex = math.huge
		local firstpointmanID
		for unitID, posIndex in pairs(firstGroup) do
			if posIndex < firstminIndex then
				firstminIndex = posIndex 
				firstpointmanID = unitID
			end
		end
		local firstpointX, firstpointY, firstpointZ = Spring.GetUnitPosition(firstpointmanID)
		local firstGroupPointMan = Vec3(firstpointX, firstpointY, firstpointZ)
		
		local secondminIndex = math.huge
		local secondpointmanID
		for unitID, posIndex in pairs(secondGroup) do
			if posIndex < secondminIndex then
				secondminIndex = posIndex 
				secondpointmanID = unitID
			end
		end
		local secondpointX, secondpointY, secondpointZ = Spring.GetUnitPosition(secondpointmanID)
		local secondGroupPointMan = Vec3(secondpointX, secondpointY, secondpointZ)
		
		local thirdminIndex = math.huge
		local thirdpointmanID
		for unitID, posIndex in pairs(thirdGroup) do
			if posIndex < thirdminIndex then
				thirdminIndex = posIndex 
				thirdpointmanID = unitID
			end
		end
		local thirdpointX, thirdpointY, thirdpointZ = Spring.GetUnitPosition(thirdpointmanID)
		local thirdGroupPointMan = Vec3(thirdpointX, thirdpointY, thirdpointZ)
		
		local fourthminIndex = math.huge
		local fourthpointmanID
		for unitID, posIndex in pairs(fourthGroup) do
			if posIndex < fourthminIndex then
				fourthminIndex = posIndex 
				fourthpointmanID = unitID
			end
		end
		local fourthpointX, fourthpointY, fourthpointZ = Spring.GetUnitPosition(fourthpointmanID)
		local fourthGroupPointMan = Vec3(fourthpointX, fourthpointY, fourthpointZ)
		
		--Calculate the next positions
		
		local newWantedHeading = (closestMetalSpot - bb.convoyPointmanPos):ToHeading()
		
		local upFormationDefinition = Sensors.nota_jd_convoy.teamFormationLine()
		local upFormationRelative = Sensors.formation.StaticTransform(upFormationDefinition.positions, newWantedHeading, nil, upFormationDefinition.defaults.spacing)
		local upRelativePosition = (squadFormation.up.position):RotateByHeading(bb.convoyHeading)
		local nextUpPosition = bb.convoyPointmanPos + upFormationRelative[1] + upRelativePosition
		
		local bottomFormationDefinition = Sensors.nota_jd_convoy.teamFormationLine()
		local bottomFormationRelative = Sensors.formation.StaticTransform(bottomFormationDefinition.positions, newWantedHeading, nil, bottomFormationDefinition.defaults.spacing)
		local bottomRelativePosition = (squadFormation.bottom.position):RotateByHeading(bb.convoyHeading)
		local nextBottomPosition = bb.convoyPointmanPos + bottomFormationRelative[1] + bottomRelativePosition
		
		local rightFormationDefinition = Sensors.nota_jd_convoy.teamFormationColumn()
		local rightFormationRelative = Sensors.formation.StaticTransform(rightFormationDefinition.positions, newWantedHeading, nil, rightFormationDefinition.defaults.spacing)
		local rightRelativePosition = (squadFormation.right.position):RotateByHeading(bb.convoyHeading)
		local nextRightPosition = bb.convoyPointmanPos + rightFormationRelative[1] + rightRelativePosition
		
		local leftFormationDefinition = Sensors.nota_jd_convoy.teamFormationColumn()
		local leftFormationRelative = Sensors.formation.StaticTransform(leftFormationDefinition.positions, newWantedHeading, nil, leftFormationDefinition.defaults.spacing)
		local leftRelativePosition = (squadFormation.left.position):RotateByHeading(bb.convoyHeading)
		local nextLeftPosition = bb.convoyPointmanPos + leftFormationRelative[1] + leftRelativePosition
		
		--Establish arrays of the pointmen position and the groups
		
		local pointmenPositionsArray = {firstGroupPointMan, secondGroupPointMan, thirdGroupPointMan, fourthGroupPointMan}
		local originalGroupArray = {firstGroup, secondGroup, thirdGroup, fourthGroup}
		
		--Find the upper group
		
		local upMinDistance = math.huge
		local upBestGroup
		local upBestIndice 
		
		for i=1, 4 do
			if nextUpPosition:Distance(pointmenPositionsArray[i]) < upMinDistance then 
				upMinDistance = nextUpPosition:Distance(pointmenPositionsArray[i])
				upBestGroup = originalGroupArray[i]
				upBestIndice = i
			end
		end
		
		pointmenPositionsArray[upBestIndice] = Vec3(math.huge, math.huge, math.huge)
		
		--Find the bottom group
		
		local bottomMinDistance = math.huge
		local bottomBestGroup
		local bottomBestIndice 
		
		for i=1, 4 do
			if nextBottomPosition:Distance(pointmenPositionsArray[i]) < bottomMinDistance then 
				bottomMinDistance = nextBottomPosition:Distance(pointmenPositionsArray[i])
				bottomBestGroup = originalGroupArray[i]
				bottomBestIndice = i
			end
		end
		
		pointmenPositionsArray[bottomBestIndice] = Vec3(math.huge, math.huge, math.huge)
			
		--Find the right group
		
		local rightMinDistance = math.huge
		local rightBestGroup
		local rightBestIndice 
		
		for i=1, 4 do
			if nextRightPosition:Distance(pointmenPositionsArray[i]) < rightMinDistance then 
				rightMinDistance = nextRightPosition:Distance(pointmenPositionsArray[i])
				rightBestGroup = originalGroupArray[i]
				rightBestIndice = i
			end
		end
		
		pointmenPositionsArray[rightBestIndice] = Vec3(math.huge, math.huge, math.huge)
		
		--Find the left group
		
		local leftMinDistance = math.huge
		local leftBestGroup
		local leftBestIndice 
		
		for i=1, 4 do
			if nextLeftPosition:Distance(pointmenPositionsArray[i]) < leftMinDistance then 
				leftMinDistance = nextLeftPosition:Distance(pointmenPositionsArray[i])
				leftBestGroup = originalGroupArray[i]
				leftBestIndice = i
			end
		end
		
		pointmenPositionsArray[leftBestGroup] = Vec3(math.huge, math.huge, math.huge)
		
		--Difne the result to return
		local groupArray = {upBestGroup, bottomBestGroup, rightBestGroup, leftBestGroup}
	
		return groupArray
	end  
end