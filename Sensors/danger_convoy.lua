local sensorInfo = {
	name = "Form_InfantryGroup",
	desc = "Form 4 equivalent groups",
	author = "DeslogisJocelyn",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching
local HEALTH_IF_UNKNOWN = 300

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back
-- @argument sourceFormation [array of Vec3] list of positions representing input formaiton  
-- @argument heading [number,Vec3] heading or position vector towards which we want to rotate the formation, Vec3 makes sense only if center position is used as well
-- @argument centerPosition [Vec3|optional] if passed, it means we want to get absolute positions on return with this position as center of the formation
-- @argument spacing [Vec3|optional] if passed, represents modification of norm source position in each dimension
return function()

	local radiusRadar = 1000
	
	local allUnitsAround = Spring.GetUnitsInSphere(bb.convoyPointmanPos.x, bb.convoyPointmanPos.y, bb.convoyPointmanPos.z, radiusRadar)
	
	local UpDanger = 0
	local BottomDanger = 0
	local RightDanger = 0
	local LeftDanger = 0
	
	local sideInDanger = 0
	
	for i=1, #allUnitsAround do
		local thisUnitID = allUnitsAround[i]
				
		local isAllied = Spring.IsUnitAllied(thisUnitID)
		local health = Spring.GetUnitHealth(thisUnitID)
		if (health == nil) then health = HEALTH_IF_UNKNOWN end
		
		if (not isAllied) then
			local x,y,z = Spring.GetUnitPosition(thisUnitID)
			local unitPosition = Vec3(x,y,z)
			local unitHeading = (unitPosition - bb.convoyPointmanPos):ToHeading()
			
			if (bb.convoyHeading - unitHeading) < 135 and (bb.convoyHeading - unitHeading) > 45  or (bb.convoyHeading - unitHeading) < -225 and (bb.convoyHeading - unitHeading) > -315 then
				LeftDanger = LeftDanger + health
			elseif (bb.convoyHeading - unitHeading) < 225 and (bb.convoyHeading - unitHeading) > 135  or (bb.convoyHeading - unitHeading) < -135 and (bb.convoyHeading - unitHeading) > -225 then
				BottomDanger = BottomDanger + health
			elseif (bb.convoyHeading - unitHeading) < 315 and (bb.convoyHeading - unitHeading) > 225  or (bb.convoyHeading - unitHeading) < -45 and (bb.convoyHeading - unitHeading) > -135 then
				RightDanger = RightDanger + health
			else
				UpDanger = UpDanger + health
			end
			
		end
	end
	
	if UpDanger > 0 then sideInDanger = sideInDanger + 1 end
	if BottomDanger > 0 then sideInDanger = sideInDanger + 1 end
	if RightDanger > 0 then sideInDanger = sideInDanger + 1 end
	if LeftDanger > 0 then sideInDanger = sideInDanger + 1 end
	
	return {sideInDanger, UpDanger, BottomDanger, RightDanger, LeftDanger}
end