local sensorInfo = {
	name = "closestEnemyDirection",
	desc = "Returns azimuth of the closest enemy",
	author = "PepeAmpere",
	date = "2017-06-12",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- frames

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description Return table with different context HP counts
-- @argument area [table] {center = Vec3, radius = number} 
return function(area)
	local center = area.center
	local radius = area.radius
	
	local minDistance = math.huge
	local closestUnitPosition = Vec3(0,0,0)
	local allUnitsAround = Spring.GetUnitsInSphere(center.x, center.y, center.z, radius)
	
	for i=1, #allUnitsAround do
		local thisUnitID = allUnitsAround[i]
				
		local isAllied = Spring.IsUnitAllied(thisUnitID)
		if (not isAllied) then
			local x,y,z = Spring.GetUnitPosition(thisUnitID)
			local unitPosition = Vec3(x,y,z)
			local distance = center:Distance(unitPosition)
			if (distance < minDistance) then
				closestUnitPosition = unitPosition
				minDistance = distance
			end
		end
	end
	
	return (closestUnitPosition - center):ToHeading()
end