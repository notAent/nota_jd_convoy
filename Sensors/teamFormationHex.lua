local sensorInfo = {
	name = "teamFormationHex",
	desc = "Return hexy team formation",
	author = "PepeAmpere",
	date = "2017-11-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge -- cached forev4r

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return stuctured description of the formation
return function(limit)
	if limit == nil then limit = 50 end
	
	local SCALE = 25
	local positions = {}
	local position = 2
	local side = 1
	local inside = 0
	local smallSizeOfHex = 3 * SCALE
	local bigSizeOfHex = 2 * smallSizeOfHex	
	
	--- reset generationMoveTab
	local hexGenerationMove = {
		[1] = Vec3(smallSizeOfHex, 0, -bigSizeOfHex*math.sin(30)),
		[2] = Vec3(-smallSizeOfHex, 0, -bigSizeOfHex*math.sin(30)),
		[3] = Vec3(-bigSizeOfHex, 0, 0),
		[4] = Vec3(-smallSizeOfHex, 0, bigSizeOfHex*math.sin(30)),
		[5] = Vec3(smallSizeOfHex, 0, bigSizeOfHex*math.sin(30)),
		[6] = Vec3(bigSizeOfHex,0, 0),
	}
	---
	
	for i=1,30 do
		local startPoint = {
			[1]  = hexGenerationMove[5] * side,
			[2]  = hexGenerationMove[6] * side,
			[3]  = hexGenerationMove[1] * side,
			[4]  = hexGenerationMove[2] * side,
			[5]  = hexGenerationMove[3] * side,
			[6]  = hexGenerationMove[4] * side,
		}
		for k=1,6 do
			positions[#positions + 1] = startPoint[k]
		end
		for a=1,inside do
			for b=1,6 do
				positions[#positions + 1] = Vec3(
					startPoint[b].x + hexGenerationMove[b].x * a,
					0,
					startPoint[b].z + hexGenerationMove[b].z * a
				)
			end
		end
		side = side + 1
		inside = inside + 1
		if (#positions >= limit) then 
			break
		end
	end
	
	return {
		name = "teamFormationHex",
		positions = positions,
		generated = true,
		defaults = {
			spacing = Vec3(1, 1, 1),
			hillyCoeficient = 20,
			constrained = true,
			variant = false,
			rotable = true,
		},
	}
end