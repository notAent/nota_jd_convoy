local sensorInfo = {
	name = "teamFormationLine",
	desc = "Return team formation line",
	author = "PepeAmpere",
	date = "2017-11-10",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge -- cached forev4r

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return stuctured description of the formation
return function()
	return {
		name = "teamFormationLine",
		positions = {
			[1] = Vec3(0,0,-1),
			[2] = Vec3(-1,0,0), [3] = Vec3(1,0,0),
			[4] = Vec3(-2,0,0), [5] = Vec3(2,0,0),
			[6] = Vec3(-3,0,0), [7] = Vec3(3,0,0),
			[8] = Vec3(-4,0,0), [9] = Vec3(4,0,0),
			[10] = Vec3(-5,0,0), [11] = Vec3(5,0,0),
			[12] = Vec3(-6,0,0), [13] = Vec3(6,0,0),
			[14] = Vec3(-7,0,0), [15] = Vec3(7,0,0),
			[16] = Vec3(0,0,0),
			[17] = Vec3(-1,0,-1), [18]  = Vec3(1,0,-1), 
			[19] = Vec3(-2,0,-1), [20]  = Vec3(2,0,-1), 
			[21] = Vec3(-3,0,-1), [22] = Vec3(3,0,-1),
			[23] = Vec3(-4,0,-1), [24] = Vec3(4,0,-1),
			[25] = Vec3(-5,0,-1), [26] = Vec3(5,0,-1),
			[27] = Vec3(-6,0,-1), [28] = Vec3(6,0,-1),
			[29] = Vec3(-7,0,-1), [30] = Vec3(7,0,-1),
			[30] = Vec3(0,0,1),
			[31] = Vec3(-1,0,1), [32]  = Vec3(1,0,1), 
			[33] = Vec3(-2,0,1), [34]  = Vec3(2,0,1), 
			[35] = Vec3(-3,0,1), [36] = Vec3(3,0,1),
			[37] = Vec3(-4,0,1), [38] = Vec3(4,0,1),
			[39] = Vec3(-5,0,1), [40] = Vec3(5,0,1),
			[41] = Vec3(-6,0,1), [42] = Vec3(6,0,1),
			[43] = Vec3(-7,0,1), [44] = Vec3(7,0,1),
		},		
		generated = false,
		defaults = {
			spacing = Vec3(50, 1 ,50),
			hillyCoeficient = 20,
			constrained = true,
			variant = false,
			rotable = true,
		},
	}
end