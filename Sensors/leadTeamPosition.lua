local sensorInfo = {
	name = "leadTeamPosition",
	desc = "Position of squad",
	author = "PepeAmpere",
	date = "2017-11-12",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


return function(customGroup, closestMetalSpot)
	-- choose the pointmen
	local minIndex = math.huge
	local pointmanID
	for unitID, posIndex in pairs(customGroup) do
		if posIndex < minIndex then
			minIndex = posIndex 
			pointmanID = unitID
		end
	end
	
	local pointX, pointY, pointZ = Spring.GetUnitPosition(pointmanID)
	if (pointX ~= nil) then
		bb.convoyPointmanPos = Vec3(pointX, pointY, pointZ)
		
		-- if (bb.convoyPointmanPos:Distance(closestMetalSpot) > 10) then
		bb.convoyHeading = (closestMetalSpot - bb.convoyPointmanPos):ToHeading()
		-- end
		
		return bb.convoyPointmanPos
	end
	return nil
end