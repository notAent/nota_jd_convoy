local sensorInfo = {
	name = "isOneSideWeak",
	desc = "Form 4 equivalent groups",
	author = "DeslogisJocelyn",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back
-- @argument sourceFormation [array of Vec3] list of positions representing input formaiton  
-- @argument heading [number,Vec3] heading or position vector towards which we want to rotate the formation, Vec3 makes sense only if center position is used as well
-- @argument centerPosition [Vec3|optional] if passed, it means we want to get absolute positions on return with this position as center of the formation
-- @argument spacing [Vec3|optional] if passed, represents modification of norm source position in each dimension
return function(firstGroup, secondGroup, thirdGroup, fourthGroup)

	local needToReorganize = false

	local GroupsHP = {0, 0, 0, 0}
	
	--Calculate the strength of each group
	
	for unitID, posIndex in pairs(firstGroup) do
		if (Spring.GetUnitIsDead(unitID) ~= nil) and (not Spring.GetUnitIsDead(unitID)) then
			GroupsHP[1] = GroupsHP[1] + Spring.GetUnitHealth(unitID)
		end
	end
	
	for unitID, posIndex in pairs(secondGroup) do
		if (Spring.GetUnitIsDead(unitID) ~= nil) and (not Spring.GetUnitIsDead(unitID)) then
			GroupsHP[2] = GroupsHP[2] + Spring.GetUnitHealth(unitID)
		end
	end
	
	for unitID, posIndex in pairs(thirdGroup) do
		if (Spring.GetUnitIsDead(unitID) ~= nil) and (not Spring.GetUnitIsDead(unitID)) then
			GroupsHP[3] = GroupsHP[3] + Spring.GetUnitHealth(unitID)
		end
	end
	
	for unitID, posIndex in pairs(fourthGroup) do
		if (Spring.GetUnitIsDead(unitID) ~= nil) and (not Spring.GetUnitIsDead(unitID)) then
			GroupsHP[4] = GroupsHP[4] + Spring.GetUnitHealth(unitID)
		end
	end
	
	--Calculate the biggest group
	
	local bestGroupIndice = 1
	for i = 1, 4 do
		if GroupsHP[bestGroupIndice] < GroupsHP[i] then
			bestGroupIndice = i
		end
	end
	
	--Check if no group is under a quqrter size of the biggest group
	
	for i = 1, 4 do
		if  GroupsHP[i] < (GroupsHP[bestGroupIndice] / 4) then
			needToReorganize = true
		end
	end
	
	
	return needToReorganize
end