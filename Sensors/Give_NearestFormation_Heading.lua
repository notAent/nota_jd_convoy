local sensorInfo = {
	name = "Form_InfantryGroup",
	desc = "Form 4 equivalent groups",
	author = "DeslogisJocelyn",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back
-- @argument sourceFormation [array of Vec3] list of positions representing input formaiton  
-- @argument heading [number,Vec3] heading or position vector towards which we want to rotate the formation, Vec3 makes sense only if center position is used as well
-- @argument centerPosition [Vec3|optional] if passed, it means we want to get absolute positions on return with this position as center of the formation
-- @argument spacing [Vec3|optional] if passed, represents modification of norm source position in each dimension
return function(firstGroup, secondGroup, thirdGroup, fourthGroup, closestMetalSpot)
	
	if (bb.convoyHeading == nil) then 
		
		local groupArray = {firstGroup, secondGroup, thirdGroup, fourthGroup}
		
		return groupArray
	else
	
		--Calculate the next heading
		
		local newWantedHeading = (closestMetalSpot - bb.convoyPointmanPos):ToHeading()
		local upGroup, bottomroup, rightGroup, leftGroup
		
		if (bb.convoyHeading - newWantedHeading) < 135 and (bb.convoyHeading - newWantedHeading) > 45  or (bb.convoyHeading - newWantedHeading) < -225 and (bb.convoyHeading - newWantedHeading) > -315 then
			upGroup = fourthGroup
			bottomroup = thirdGroup
			rightGroup = firstGroup
			leftGroup = secondGroup
			--Spring.Echo("LeftMove")
		
		elseif (bb.convoyHeading - newWantedHeading) < 225 and (bb.convoyHeading - newWantedHeading) > 135  or (bb.convoyHeading - newWantedHeading) < -135 and (bb.convoyHeading - newWantedHeading) > -225 then
			upGroup = secondGroup
			bottomroup = firstGroup
			rightGroup = fourthGroup
			leftGroup = thirdGroup
			--Spring.Echo("BottomMove")
		
		elseif (bb.convoyHeading - newWantedHeading) < 315 and (bb.convoyHeading - newWantedHeading) > 225  or (bb.convoyHeading - newWantedHeading) < -45 and (bb.convoyHeading - newWantedHeading) > -135 then
			upGroup = thirdGroup
			bottomroup = fourthGroup
			rightGroup = secondGroup
			leftGroup = firstGroup
			--Spring.Echo("RightMove")
		
		else
			upGroup = firstGroup
			bottomroup = secondGroup
			rightGroup = thirdGroup
			leftGroup = fourthGroup
			--Spring.Echo("UpMove")
		
		end
		
		local groupArray = {upGroup, bottomroup, rightGroup, leftGroup}
	
		return groupArray
	end  
end