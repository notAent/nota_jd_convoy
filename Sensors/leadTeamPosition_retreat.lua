local sensorInfo = {
	name = "leadTeamPosition_retreat",
	desc = "Position of squad",
	author = "JocelynDeslogis",
	date = "2017-11-12",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


return function(constructorGroup, retreatPosition, groupDanger)

	-- update the position of the convoy
	
	local minIndex = math.huge
	local pointmanID
	for unitID, posIndex in pairs(constructorGroup) do
		if posIndex < minIndex then
			minIndex = posIndex 
			pointmanID = unitID
		end
	end
	local pointX, pointY, pointZ = Spring.GetUnitPosition(pointmanID)
	if pointX ~= nil then
		bb.convoyPointmanPos = Vec3(pointX, pointY, pointZ)
	
		-- Calculate the final heading to reach
		
		local retreatHeading = (retreatPosition - bb.convoyPointmanPos):ToHeading()
		
		-- Find the weakest side
		
		local weakestThreat = 0
		local valueWeakestThreat = math.huge
		for i = 2, 5 do
			if groupDanger[i] < valueWeakestThreat then
				valueWeakestThreat = groupDanger[i]
				weakestThreat = i-1
			end
		end
		
		--Find the strongest side
		
		local valueBiggestThreat = math.huge
		for i = 2, 5 do
			if groupDanger[i] > valueBiggestThreat then
				valueBiggestThreat = groupDanger[i]
			end
		end
		
		--Caculate the heading of the weakest threat
		local weakestEnemyHeading
		if (weakestThreat == 1) or (weakestThreat == 4) then
			weakestEnemyHeading = (weakestThreat -1) * 90
		elseif (weakestThreat == 2) then
			weakestEnemyHeading = weakestThreat * 90
		else
			weakestEnemyHeading = (weakestThreat - 2) * 90
		end
		
		local freeWayCoefficient =  valueWeakestThreat / valueBiggestThreat --if side is free, retreat heading becomes the main heading
		
		-- update the heading of convoy
		
		bb.convoyHeading = (1 - freeWayCoefficient) * (bb.convoyHeading + weakestEnemyHeading) + freeWayCoefficient * retreatHeading
		
		return bb.convoyPointmanPos
	end
	return nil
end