local sensorInfo = {
	name = "Form_InfantryGroup",
	desc = "Form 4 equivalent groups",
	author = "DeslogisJocelyn",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back
-- @argument sourceFormation [array of Vec3] list of positions representing input formaiton  
-- @argument heading [number,Vec3] heading or position vector towards which we want to rotate the formation, Vec3 makes sense only if center position is used as well
-- @argument centerPosition [Vec3|optional] if passed, it means we want to get absolute positions on return with this position as center of the formation
-- @argument spacing [Vec3|optional] if passed, represents modification of norm source position in each dimension
return function(units)

	local groupSize = math.floor(#units / 4)
	
	local firstGroup = {}
	local secondGroup = {}
	local thirdGroup = {}
	local fourthGroup = {}
	
	local j = 1
	for i = 1, groupSize do
		firstGroup[units[i]] = j
		j = j + 1
	end
	
	local j = 1
	for i = groupSize + 1, 2*groupSize do
		secondGroup[units[i]] = j
		j = j + 1
	end
	
	local j = 1
	for i = 2*groupSize + 1, 3*groupSize do
		thirdGroup[units[i]] = j
		j = j + 1
	end
	
	local j = 1
	for i = 3*groupSize + 1, #units do
		fourthGroup[units[i]] = j
		j = j + 1
	end
	
	local groupArray = {firstGroup, secondGroup, thirdGroup, fourthGroup}
	
	return groupArray  
end