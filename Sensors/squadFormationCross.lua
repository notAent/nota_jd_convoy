local sensorInfo = {
	name = "squadFormationCross",
	desc = "Return squad formation Cross",
	author = "PepeAmpere",
	date = "2017-11-10",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return stuctured description of the formation
return function()
	return {
		up = {
			position = Vec3(0,0,-700),
			teamFormationHeading = 180,
			teamFormationName = "teamFormationLine",
		},
		bottom = {
			position = Vec3(0,0,300),
			teamFormationHeading = 0,
			teamFormationName = "teamFormationLine",
		},
		right = {
			position = Vec3(400,0,0),
			teamFormationHeading = 0,
			teamFormationName = "teamFormationColumn",
		},
		left = {
			position = Vec3(-400,0,0),
			teamFormationHeading = 180,
			teamFormationName = "teamFormationColumn",
		},
		center = {
			position = Vec3(0,0,0),
			teamFormationHeading = 0,
			teamFormationName = "teamFormationHex",
		},
		upCavalry = {
			position = Vec3(0,0,-550),
			teamFormationHeading = 0,
			teamFormationName = "teamFormationLine_cavalry",
		},
		bottomCavalry = {
			position = Vec3(0,0,550),
			teamFormationHeading = 0,
			teamFormationName = "teamFormationLine_cavalry",
		},
		rightCavalry = {
			position = Vec3(300,0,0),
			teamFormationHeading = 0,
			teamFormationName = "teamFormationColumn_cavalry",
		},
		leftCavalry = {
			position = Vec3(-300,0,0),
			teamFormationHeading = 0,
			teamFormationName = "teamFormationColumn_cavalry",
		},
		upCavalry = {
			position = Vec3(0,0,-550),
			teamFormationHeading = 0,
			teamFormationName = "teamFormationLine_cavalry",
		},
		upAA = {
			position = Vec3(0,0,-350),
			teamFormationHeading = 0,
			teamFormationName = "teamFormationLine_AA",
		},
	}
end