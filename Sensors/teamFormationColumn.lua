local sensorInfo = {
	name = "teamFormationColumn",
	desc = "Return team formation column",
	author = "PepeAmpere",
	date = "2017-11-10",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge -- cached forev4r

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return stuctured description of the formation
return function()
	return {
		name = "teamFormationColumn",
		positions = {
			[1] = Vec3(-1,0,0),
			[2] = Vec3(0,0,-1), [3] = Vec3(0,0,1),
			[4] = Vec3(0,0,-2), [5] = Vec3(0,0,2),
			[6] = Vec3(0,0,-3), [7] = Vec3(0,0,3),
			[8] = Vec3(0,0,-4), [9] = Vec3(0,0,4),
			[10] = Vec3(0,0,-5), [11] = Vec3(0,0,5),
			[12] = Vec3(0,0,-6), [13] = Vec3(0,0,6),
			[14] = Vec3(0,0,-7), [15] = Vec3(0,0,7),
			[16] = Vec3(0,0,0),
			[17] = Vec3(-1,0,-1), [18]  = Vec3(-1,0,1), 
			[19] = Vec3(-1,0,-2), [20]  = Vec3(-1,0,2), 
			[21] = Vec3(-1,0,-3), [22] = Vec3(-1,0,3),
			[23] = Vec3(-1,0,-4), [24] = Vec3(-1,0,4),
			[25] = Vec3(-1,0,-5), [26] = Vec3(-1,0,5),
			[27] = Vec3(-1,0,-6), [28] = Vec3(-1,0,6),
			[29] = Vec3(-1,0,-7), [30] = Vec3(-1,0,7),
			[30] = Vec3(0,0,1),
			[31] = Vec3(1,0,-1), [32]  = Vec3(1,0,1), 
			[33] = Vec3(1,0,-2), [34]  = Vec3(1,0,2), 
			[35] = Vec3(1,0,-3), [36] = Vec3(1,0,3),
			[37] = Vec3(1,0,-4), [38] = Vec3(1,0,4),
			[39] = Vec3(1,0,-5), [40] = Vec3(1,0,5),
			[41] = Vec3(1,0,-6), [42] = Vec3(1,0,6),
			[43] = Vec3(1,0,-7), [44] = Vec3(1,0,7),
		},		
		generated = false,
		defaults = {
			spacing = Vec3(50, 1, 50),
			hillyCoeficient = 20,
			constrained = true,
			variant = false,
			rotable = true,
		},
	}
end