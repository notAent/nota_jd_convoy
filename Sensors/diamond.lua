local sensorInfo = {
	name = "teamFormationColumn",
	desc = "Return team formation column",
	author = "PepeAmpere",
	date = "2017-11-10",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge -- cached forev4r

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return stuctured description of the formation
return function()
	return {
		name = "diamond",
		positions = {
			[1] = Vec3(0,0,0),
			[2] = Vec3(1,0,0), [3] = Vec3(0,0,-1),
			[4] = Vec3(-1,0,0), [5] = Vec3(0,0,1),
			[6] = Vec3(1,0,-1), [7] = Vec3(-1,0,-1),
			[8] = Vec3(-1,0,1), [9] = Vec3(1,0,1),
			[10] = Vec3(0,0,2), [11] = Vec3(2,0,0),
			[12] = Vec3(0,0,-2), [13] = Vec3(-2,0,0),
		},		
		generated = false,
		defaults = {
			spacing = Vec3(50, 1 ,50),
			hillyCoeficient = 20,
			constrained = true,
			variant = false,
			rotable = true,
		},		
	}
end
