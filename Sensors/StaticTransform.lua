local sensorInfo = {
	name = "StaticTransform",
	desc = "Do some transformations over passed static formation and return it back",
	author = "PepeAmpere",
	date = "2017-05-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back
-- @argument sourceFormation [array of Vec3] list of positions representing input formaiton  
-- @argument heading [number,Vec3] heading or position vector towards which we want to rotate the formation, Vec3 makes sense only if center position is used as well
-- @argument centerPosition [Vec3|optional] if passed, it means we want to get absolute positions on return with this position as center of the formation
-- @argument spacing [Vec3|optional] if passed, represents modification of norm source position in each dimension
return function(sourceFormation, heading, centerPosition, spacing)
	-- check bad inputs
	if (type(heading) == "table" and centerPosition == nil) then 
		Logger.warn("formation.StaticTransform", "Argument #2 [heading] was defined by Vec3 but argument #3 [centerPosition] is not defined. Do you really want to rotate you formation around top-left corner of the map?") 
	end
	
	-- default overrides if needed
	if (centerPosition == nil) then centerPosition = Vec3(0,0,0) end
	if (spacing == nil) then spacing = Vec3(1,1,1) end
	
	-- processing of heading defined by target position
	if (type(heading) == "table") then heading = (heading - centerPosition):ToHeading() end
	
	-- do transformations
	local newFormation = {}
	local newFormationCounter = 0
	
	-- if just heading, optimization condition
	if (centerPosition == Vec3(0,0,0) and spacing == Vec3(1,1,1)) then
		for i=1, #sourceFormation do
			newFormationCounter = newFormationCounter + 1
			newFormation[newFormationCounter] = sourceFormation[i]:RotateByHeading(heading) -- clockwise rotation as the formationDefition
		end
	else -- any more complex request is not more optimized
		for i=1, #sourceFormation do
			newFormationCounter = newFormationCounter + 1
			newFormation[newFormationCounter] = (sourceFormation[i] * spacing):RotateByHeading(heading) + centerPosition -- clockwise rotation as the formationDefition
		end
	end
	
	return newFormation
end