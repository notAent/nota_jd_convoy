local sensorInfo = {
	name = "Form_InfantryGroup",
	desc = "Form 4 equivalent groups",
	author = "DeslogisJocelyn",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

local function oppositeGroup(groupIndice)
	if (groupIndice == 1) or (groupIndice == 3) then
		return groupIndice + 1
	else
		return groupIndice - 1
	end
end

local function freeUnit(groupDanger)
	local freeUnit = 0
	for i = 2, 5 do
		if groupDanger[i] == 0 then 
		freeUnit = i
		end
	end
	return freeUnit
end

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back
-- @argument sourceFormation [array of Vec3] list of positions representing input formaiton  
-- @argument heading [number,Vec3] heading or position vector towards which we want to rotate the formation, Vec3 makes sense only if center position is used as well
-- @argument centerPosition [Vec3|optional] if passed, it means we want to get absolute positions on return with this position as center of the formation
-- @argument spacing [Vec3|optional] if passed, represents modification of norm source position in each dimension
return function(groupDanger)
	
	--Calculate the two biggest threats
	local biggestThreat = 0
	local secondBiggestThreat = 0
	local valueBiggestThreat = 0
	local valueSecondBiggestThreat = 0
	for i = 2, 5 do
		if groupDanger[i] > valueBiggestThreat then
			valueSecondBiggestThreat = valueBiggestThreat
			secondBiggestThreat = biggestThreat
			valueBiggestThreat = groupDanger[i]
			biggestThreat = i-1
		elseif groupDanger[i] > valueSecondBiggestThreat then
			valueSecondBiggestThreat = groupDanger[i]
			secondBiggestThreat = i-1
		end
	end
	
	--Find the infantry group which need to move
	local infantryGroupToMove
	local oppositeSecondThreat = oppositeGroup(secondBiggestThreat)
	if groupDanger[oppositeSecondThreat + 1] == 0 then -- Test if the opposite of the second threat is in danger
		infantryGroupToMove = oppositeSecondThreat 
	else -- send the unit in free sector
		infantryGroupToMove = freeUnit(groupDanger) - 1
	end
	
	return {biggestThreat, secondBiggestThreat, infantryGroupToMove}  
	-- first argument is the position to go for cavalry
	--second argument is the infantry group to move
	--third argumant is the position to go for infantry
end