function getInfo()
	return {
		fields = { "pos" }
	}
end

local function distanceSquare(p1, p2)
	return (p1.x - p2.x)*(p1.x - p2.x) + (p1.z - p2.z)*(p1.z - p2.z)
end

local function isNotOccupied(metalSpot)
	local MEX_DEFID = UnitDefNames["armmex"].id
	local empty = false
	local thisSpotPos = metalSpot
	
	local blockingSpot = Spring.TestBuildOrder(MEX_DEFID, thisSpotPos.x, thisSpotPos.y, thisSpotPos.z, 1)
	if blockingSpot > 0 then			
		empty = true
	end
	
	return empty
end


return function(posPointMan)

	local metalPositions =  Sensors.map.metalSpots()
	
	local center = posPointMan 
	
	if(not metalPositions or not center)then
		return nil
	end
	local selectedMetal, minDistance
	
	for i = 1, #metalPositions do
		local pos = metalPositions[i]
			local distance = distanceSquare({ x = pos.x, z = pos.z }, center)
			if (minDistance == nil or distance < minDistance) and (isNotOccupied(metalPositions[i])) and (math.abs(posPointMan.y - pos.y) < 100) then
				selectedMetal = pos
				minDistance = distance
			end
	end
	
	return selectedMetal
end