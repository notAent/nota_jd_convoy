local sensorInfo = {
	name = "PositionCustomGroup",
	desc = "Return position of the point unit from custom definition",
	author = "PepeAmpere",
	date = "2017-11-10",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description return static position of the first unit
return function(group)
	-- choose the pointmen
	local minIndex = math.huge
	local minIndexID = 0
	for k,v in pairs(group) do
		if v < minIndex then
		minIndex = v 
		minIndexID = k
		end
	end
	local pointman = minIndexID -- while this is running, we know that #units > 0, so pointman is valid
	local pointX, pointY, pointZ = SpringGetUnitPosition(pointman)
	return Vec3(pointX, pointY, pointZ)
end