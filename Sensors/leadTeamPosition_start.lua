local sensorInfo = {
	name = "leadTeamPosition",
	desc = "Position of squad",
	author = "PepeAmpere",
	date = "2017-11-12",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


return function(closestMetalSpot, retreatPosition)
	
	-- Start pos is the reference
	bb.convoyPointmanPos = retreatPosition
	
	bb.convoyHeading = (closestMetalSpot - bb.convoyPointmanPos):ToHeading()
	
	return bb.convoyPointmanPos
end