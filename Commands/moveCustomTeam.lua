function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "groupDefintion",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "relativePosition",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "formation", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<relative formation>",
			},
			{ 
				name = "pointManInPosition", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local customGroup = parameter.groupDefintion -- table
	local position = bb.convoyPointmanPos + parameter.relativePosition -- Vec3
	local formation = parameter.formation -- array of Vec3
	local pointManInPosition = parameter.pointManInPosition -- boolean
	local fight = parameter.fight -- boolean

	--Spring.Echo(dump(parameter.formation))
	
	-- validation
	-- if (#units > #formation) then
		-- Logger.warn("formation.move", "Your formation size [" .. #formation .. "] is smaller than needed for given count of units [" .. #units .. "] in this group.") 
		-- return FAILURE
	-- end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	-- choose the pointmen
	local minIndex = math.huge
	local pointmanID
	for unitID, posIndex in pairs(customGroup) do
		if (posIndex < minIndex) and (Spring.GetUnitIsDead(unitID) ~= nil) and (not Spring.GetUnitIsDead(unitID)) then
		minIndex = posIndex 
		pointmanID = unitID
		end
	end
	local pointX, pointY, pointZ = SpringGetUnitPosition(pointmanID)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	
	-- threshold of pointan success
	if (self.lastPointmanPosition ~= nil) then
		if (pointmanPosition:Distance(self.lastPointmanPosition) < 20) then 
			self.threshold = self.threshold + THRESHOLD_STEP
		else 
			self.threshold = THRESHOLD_DEFAULT
		end
	else
		self.threshold = THRESHOLD_DEFAULT
	end
		
	self.lastPointmanPosition = pointmanPosition
	
	-- check pointman success
	-- THIS LOGIC IS TEMPORARY, NOT CONSIDERING OTHER UNITS POSITION
	local pointmanOffset = formation[1]
	local pointmanWantedPosition = position + pointmanOffset
	if (pointmanPosition:Distance(pointmanWantedPosition) < self.threshold) then
		if (not pointManInPosition) then 
			return SUCCESS
		else
			return RUNNING
		end
	else
		SpringGiveOrderToUnit(pointmanID, cmdID, (position + formation[1]):AsSpringVector(), {})
			
		for unitID, posIndex in pairs(customGroup) do
			if unitID ~= pointmanID then
				local thisUnitWantedPosition = pointmanPosition - pointmanOffset + formation[posIndex]
				SpringGiveOrderToUnit(unitID, cmdID, thisUnitWantedPosition:AsSpringVector(), {})
			end
		end
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
